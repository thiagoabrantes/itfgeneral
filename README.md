# Repositório de métodos gerais da Interfusão

Esse é o repositório de métodos gerais que auxiliará na criação de tasks para a Interfusão.

*É recomendado ler esse arquivo para saber como utilizar e como contribuir com novos métodos.*

---

## Utilização

### Como Utilizar em Outros Projetos:

Copie os arquivos ITFGeneral.dll, ITFGeneral.pdb e ITFGeneral.xml que está na pasta ITFGeneral/ITFGeneral/bin/debug e colar no servidor a qual será utilizado.
Adicionar o arquivo ITFGeneral.dll nas referências do projeto.

### Como Utilizar os Métodos
#### 1. Namespace ITFFiles:

A classe ITFFileManagement contém métodos e constantes para auxiliar durante o gerenciamento de arquivos.
As constantes determinam o que vai ser feito caso já exista o arquivo no caminho de destino. As constantes estão na classe ITFFileManagement e são OVERWRITE que substitui o arquivo de destino com o arquivo de origem, ADD_INDEX que mantém o arquivo de destino e adiciona um índice ao arquivo de origem, mantendo os dois arquivos e KEEP_ORIGINAL_FILE que mantém o arquivo de destino na pasta. Caso o método não seja especificado, o arquivo de destino será mantido.
Possui dois métodos, sendo eles:

	- TryMoveTo
		Esse método espera o caminho de origem, o de destino e um método e move o arquivo de um caminho para outro. Os caminhos devem conter, inclusive, o nome dos arquivos.
		Caso o método seja bem executado, retorna a constante OK. Caso haja algum erro, retorna a mensagem de erro.
		
	- TryCopyTo
		Esse método espera o caminho de origem, o de destino e um método e copia o arquivo de um caminho para outro. Os caminhos devem conter, inclusive, o nome dos arquivos.
		Caso o método seja bem executado, retorna a constante OK. Caso haja algum erro, retorna a mensagem de erro.

#### 2. Namespace ITFDateTime:

A classe ITFDateTime contém métodos que auxiliam durante o uso da variável do tipo DateTime.
Os métodos são:

	- TryAddWorkingDays
		Esse método espera 3 parâmetros, uma data inicial, a quantidade de dias úteis que se deseja adicionar à essa data e uma variável DateTime com o resultado. O resultado é armazenado no último parâmetro enviado e o método retorna booleano com true, caso tudo tenha sido feito sem erros e false caso tenha ocorrido algum erro. Sábados e Domingos são considerados como dias não úteis nesse método.
	
	- TryAddWorkgingDays
		É um overwrite do método anterior. Ele espera um parâmetro a mais que é uma lista de DateTime com datas de feriado que não devem ser considerados dias úteis. Além dessa lista, Sábados e Domingos também não são considerados dias úteis.
		
*O método não gerencia os horários que estarão contidos nas variáveis FromDate e na lista de DateTime para os feriados. Caso haja horários inconsistentes entre as variáveis, o método considerará esse dia como dia útil. Para resolver, enviar o parâmetro DateTime com os horários zerados, assim como todas as variáveis da lista de feriados.*

---

### 3. Validadores de CPF e CNPJ 

## Contribuindo

Forkar ou clonar o repositório para o seu repositório privado com um branch nomeado como *Changes*. Realizar as alterações ou criação de novas funcionalidades e enviar como pull request.
Um avaliador irá verificar o código e, caso esteja correto, irá adicionar ao branch principal do repositório.