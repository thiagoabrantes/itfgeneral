﻿using System;
using System.Collections.Generic;

namespace ITFGeneral.ITFDateTime
{
    /// <summary>
    /// Add working days to a DateTime variable
    /// </summary>
    public class ITFDateTime
    {
        #region Methods
        /// <summary>
        /// Add working days to FromDate DateTime. Sunday and Saturday are not considered as Working Days
        /// </summary>
        /// <param name="FromDate">DateTime parameter to add working days</param>
        /// <param name="BusinessDays">Working days to add to FromDate</param>
        /// <param name="ResultDate">DateTime returned after add BusinessDays to FromDate. If it is not successfully, returns 01/01/0001 DateTime</param>
        public static bool TryAddWorkingDays(DateTime FromDate, int BusinessDays, out DateTime ResultDate)
        {
            ResultDate = FromDate;
            try
            {
                for (int i = 0; i < BusinessDays; i++)
                {
                    while (ResultDate.DayOfWeek.Equals(DayOfWeek.Saturday) || ResultDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        ResultDate = ResultDate.AddDays(1);
                    }
                    ResultDate = ResultDate.AddDays(1);
                }
                while (ResultDate.DayOfWeek.Equals(DayOfWeek.Saturday) || ResultDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    ResultDate = ResultDate.AddDays(1);
                }
                return true;
            }
            catch (Exception ex)
            {
                ResultDate = new DateTime(1, 1, 1);
                return false;
            }
        }

        /// <summary>
        /// Add working days to FromDate DateTime. Holydays List, Sunday and Saturday are not considered as Working Days.
        /// </summary>
        /// <param name="FromDate">DateTime parameter to add working days</param>
        /// <param name="BusinessDays">Working days to add to FromDate</param>
        /// <param name="Holidays">DateTime List to be considered as not working days</param>
        /// <param name="ResultDate">DateTime returned after add BusinessDays to FromDate. If it is not successfully, returns 01/01/0001 DateTime</param>
        public static bool TryAddWorkingDays(DateTime FromDate, int BusinessDays, List<DateTime> Holidays, out DateTime ResultDate)
        {
            ResultDate = FromDate;
            try
            {
                for (int i = 0; i < BusinessDays; i++)
                {
                    while (Holidays.Contains(ResultDate) || ResultDate.DayOfWeek.Equals(DayOfWeek.Saturday) || ResultDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                    {
                        ResultDate = ResultDate.AddDays(1);
                    }
                    ResultDate = ResultDate.AddDays(1);
                }
                while (Holidays.Contains(ResultDate) || ResultDate.DayOfWeek.Equals(DayOfWeek.Saturday) || ResultDate.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    ResultDate = ResultDate.AddDays(1);
                }
                return true;
            }
            catch (Exception ex)
            {
                ResultDate = new DateTime(1, 1, 1);
                return false;
            }
        }
        #endregion
    }
}
