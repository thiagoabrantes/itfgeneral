﻿using System;
using System.IO;

namespace ITFGeneral.ITFFiles
{
    /// <summary>
    /// Move or Copy files with options to do in case of a destination path already has a file with the same name
    /// </summary>
    public class ITFFileManagement
    {
        #region Constants
        /// <summary>
        /// Method Type
        /// </summary>
        public const string OVERWRITE = "Overwrite";
        /// <summary>
        /// Method Type
        /// </summary>
        public const string ADD_INDEX = "Add Index";
        /// <summary>
        /// Method Type
        /// </summary>
        public const string KEEP_ORIGINAL_FILE = "Keep Original File";
        /// <summary>
        /// Result returned if method is executed successfully
        /// </summary>
        public const string OK = "OK";
        #endregion

        #region Methods
        /// <summary>
        /// Move file from one path to another
        /// </summary>
        /// <param name="From">Origin path source</param>
        /// <param name="To">Destination path</param>
        /// <param name="Method">If already exists a file on To path, use OVERWRITE const to ovewrite the destination path, ADD_INDEX const to keep both files on destination path or KEEP_ORIGINAL_FILE const to keep the file on destination path.</param>
        /// <returns>Returns OK const if method is executed successfully</returns>
        public static string TryMoveTo(string From, string To, string Method = KEEP_ORIGINAL_FILE)
        {
            try
            {
                switch (Method)
                {
                    case OVERWRITE:
                        if (File.Exists(To))
                        {
                            File.Delete(To);
                        }
                        File.Move(From, To);
                        return OK;
                    case ADD_INDEX:
                        if (File.Exists(To))
                        {
                            int i = 1;
                            string Extension = To.Substring(To.LastIndexOf("."));
                            To = To.Replace(Extension, string.Empty);
                            To += "(1)";
                            while (File.Exists(To + Extension))
                            {
                                To = To.Replace($"({i})", $"({i + 1})");
                                i++;
                            }
                            To += Extension;
                        }
                        File.Move(From, To);
                        return OK;
                    case KEEP_ORIGINAL_FILE:
                        if (!File.Exists(To))
                        {
                            File.Move(From, To);
                        }
                        else
                        {
                            File.Delete(From);
                        }
                        return OK;
                    default:
                        throw new Exception("Method not found!");
                }
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message}";
            }
        }

        /// <summary>
        /// Copy file from one path to another
        /// </summary>
        /// <param name="From">Origin path source</param>
        /// <param name="To">Destination path</param>
        /// <param name="Method">If already exists a file on To path, use OVERWRITE const to ovewrite the destination path, ADD_INDEX const to keep both files on destination path or KEEP_ORIGINAL_FILE const to keep the file on destination path.</param>
        /// <returns>Returns OK const if method is executed successfully</returns>
        public static string TryCopyTo(string From, string To, string Method = KEEP_ORIGINAL_FILE)
        {
            try
            {
                switch (Method)
                {
                    case OVERWRITE:
                        if (File.Exists(To))
                        {
                            File.Delete(To);
                        }
                        File.Copy(From, To);
                        return OK;
                    case ADD_INDEX:
                        if (File.Exists(To))
                        {
                            int i = 1;
                            string Extension = To.Substring(To.LastIndexOf("."));
                            To = To.Replace(Extension, string.Empty);
                            To += "(1)";
                            while (File.Exists(To + Extension))
                            {
                                To = To.Replace($"({i})", $"({i + 1})");
                                i++;
                            }
                            To += Extension;
                        }
                        File.Copy(From, To);
                        return OK;
                    case KEEP_ORIGINAL_FILE:
                        if (!File.Exists(To))
                        {
                            File.Copy(From, To);
                        }
                        return OK;
                    default:
                        throw new Exception("Method not found!");
                }
            }
            catch (Exception ex)
            {
                return $"Error: {ex.Message}";
            }
        }
        #endregion
    }
}
